<!DOCTYPE html>
<?php
@session_start();

# Generate random numbers between 1 and 6
$_SESSION['anti42'] = rand(1, 6);
$_SESSION['anti43'] = rand(1, 6);
?>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta content="IE=edge" http-equiv="X-UA-Compatible">
  <meta content="width=device-width,initial-scale=1" name="viewport">
  <meta content="RMCS.fr - Radio Model Club Sénonais" name="description">
  <meta name="google" content="notranslate" />
  <meta content="Arnaud Morin" name="author">

  <!-- Disable tap highlight on IE -->
  <meta name="msapplication-tap-highlight" content="no">
  
  <link href="./assets/favicon.png" rel="icon">

  <title>RMCS.fr - Radio Model Club Sénonais</title>  

  <link href="./main.97292821.css" rel="stylesheet"></head>
  <link href="./custom.css" rel="stylesheet"></head>

  <!-- leaflet -->
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
  integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
  crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
  integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
  crossorigin=""></script>

  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        
</head>
<body>
<header>
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed">
          <span class="sr-only">Ouvrir Navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>

      <div class="navbar-collapse">
        <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="./#association" title="" class="anchor-link">Association</a></li>
            <li><a href="./#section-tt" title="" class="anchor-link">Voitures</a></li>
            <li><a href="./#section-helico" title="" class="anchor-link">Hélicos</a></li>
            <li><a href="./#section-drone" title="" class="anchor-link">Drones</a></li>
            <li><a href="./#carte" title="" class="anchor-link">Carte</a></li>
            <li><a href="./#contact-section-container" title="" class="anchor-link">Contact</a></li>
            <li>
                <a href="/forum/" target="_blank" class="btn btn-default navbar-btn" >Forum</a>
            </li>
        </ul>
      </div>
    </div>
  </nav>
</header>

<!-- Intro -->
<div class="background-image-container white-text-container" style="background-image: url('./assets/images/a.gif')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>Bienvenue au RMCS !</h2>
                <h3>Radio Model Club Sénonais</h3>
                <p class="">Voitures RC - Hélicoptères RC - Drones en immersion (FPV) à Sens de Bretagne</p>
                <a href="#association" class="btn btn-primary btn-lg anchor-link" title="">En savoir plus</a>
            </div>
        </div>
    </div>
</div>

<!-- L'asso -->
<div class="section-container" id="association">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 section-container-spacer">
                <h2 class="text-center">L'association</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <div class="fa-container">
                    <div class="fa voiture fa-3x"></div>
                </div>
                <h3 class="text-center">Voitures</h3>
                <p>Tout Terrain 1/8 ou 1/5 ? Relevez le défi et faîtes-vous plaisir sur notre piste privée !</p>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="fa-container">
                    <div class="fa heli fa-3x"></div>
                </div>
                <h3 class="text-center">Helicoptères</h3>
                <p>Plutôt amateur de grandes hélices ? Rejoignez-nous pour échanger avec nos experts !</p>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="fa-container">
                    <div class="fa drone fa-3x"></div>
                </div>
                <h3 class="text-center">Drones</h3>
                <p>Courses ou Freestyle ? Venez partager vos vols avec nous !</p>
            </div>
        </div>
    </div>
</div>

<!-- Depuis 2008 -->
<div class="section-container section-half-background-image-container">
    <div class="image-column" style="background-image: url('./assets/images/photo-0034.jpg')"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-6 text-column">
                <h3>Depuis 2008</h3>
                <p>L'aventure du Club RMCS a débuté en 2008. Cette section de l'ESSC (Entente Sénonaise Sportive et Culturelle) est le fruit de la rencontre de 3 passionnés de modélisme qui ont décidé de se réunir afin de créer un club et transmettre leur savoir-faire.</p>
                <p>C'est une excellente occasion de passer un bon moment ensemble, de partager un peu nos expériences et, surtout, de faire de beaux crashs en public ! :)</p>
            </div>
        </div>
    </div>
</div>

<!-- Section TT -->
<div class="section-container" id="section-tt">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 section-container-spacer">
                <h2 class="text-center">Section voitures TT</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="fa-container">
                    <div class="fa voiture fa-3x"></div>
                </div>
                <h4 class="text-center">Les pilotes de voitures RC sont les initiateurs de cette aventure associative et représentent toujours une force vive dans l'association.</h4>
                <h4 class="text-center">Que vous soyez électrique ou thermique, il y a de la place pour tout le monde !</h4>
            </div>
        </div>
    </div>
</div>
<div class="section-container section-half-background-image-container">
    <div class="image-column" style="background-image: url('./assets/images/1210.jpg')"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-6 text-column">
                <h3>Notre piste privée</h3>
                <p>Réalisée en 2010, elle est située en sortie de Sens, direction Feins. Sur votre gauche après "Les coursières", signalée par un petit panneau au nom du club.</p>
                <p>(voir <a href="./#carte">carte</a> ci-dessous)</p>
                <p></p>
                <p>Généralement, nos pilotes se rejoignent sur la piste le samedi après-midi (à partir de 14h) ou le dimanche. Il est judicieux de consulter le <a href="/forum/">forum</a> pour savoir qui sera présent.</p>
            </div>
        </div>
    </div>
</div>
<div class="background-image-container white-text-container" style="background-image: url('./assets/images/tt.gif')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3>Démo TT sur piste privée</h3>
            </div>
        </div>
    </div>
</div>

<!-- Section Helico -->
<div class="section-container" id="section-helico">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 section-container-spacer">
                <h2 class="text-center">Section Hélicos</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="fa-container">
                    <div class="fa heli fa-3x"></div>
                </div>
                <h4 class="text-center">La section hélicoptères a débuté peu après la création de l'association.</h4>
                <h4 class="text-center">Les membres ne sont pas seulement animés d'une passion pour le vol mais sont également férus de modélisme.</h4>
            </div>
        </div>
    </div>
</div>
<div class="section-container section-half-background-image-container">
    <div class="image-column" style="background-image: url('./assets/images/dscf0512.jpg')"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-6 text-column">
                <h3>Hélicoptères</h3>
                <p>Un hélicoptère RC en vol est très majestueux. Si, en plus, son concepteur a pris soin d'y ajouter tous les détails pour qu'il soit au plus proche du modèle original, le plaisir pour les yeux en est décuplé.</p>
                <p>Pour faire des essais, nous avons l'autorisation de voler au-dessus du terrain de foot de "la madeleine" à Sens de Bretagne (voir <a href="./#carte">carte</a> ci-dessous) tous les samedis, de 11h à 13h.</p>
                <p>De plus, lorsque les conditions météo ne sont pas idéales, un repli vers la salle de sport est possible.</p>
            </div>
        </div>
    </div>
</div>
<div class="background-image-container white-text-container" style="background-image: url('./assets/images/heli.gif')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3>Démo vol hélico</h3>
            </div>
        </div>
    </div>
</div>

<!-- Section Drones -->
<div class="section-container" id="section-drone">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 section-container-spacer">
                <h2 class="text-center">Section Drones</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="fa-container">
                    <div class="fa drone fa-3x"></div>
                </div>
                <h4 class="text-center">Depuis 2018, certains se sont essayés au vol de drones, d'abord à vue, puis en immersion.</h4>
                <h4 class="text-center">La section débute, n'hésitez pas à vous joindre à nous !</h4>
            </div>
        </div>
    </div>
</div>
<div class="section-container section-half-background-image-container">
    <div class="image-column" style="background-image: url('./assets/images/drones2.png')"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-6 text-column">
                <h3>Vol en immersion / FPV(First Person View)</h3>
                <p>Le vol avec des drones est très accessible. Leur robustesse et la faculté de les réparer soi-même sont autant d'atouts pour nos membres.</p>
                <p>Que vous ayez des connaissances en électronique ou pas, nos passionnés pourront vous aider et initier votre immersion dans le monde du FPV !</p>
                <p>Nous disposons de "gates" pour pimenter un peu les vols et ajouter un peu de challenge à ceux qui veulent faire des courses.</p>
                <p>Les vols de drones se font aux mêmes horaires et lieux que pour les hélicoptères.</p>
            </div>
        </div>
    </div>
</div>
<div class="background-image-container white-text-container" style="background-image: url('./assets/images/fpv.gif')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3>FPV / Vols en immersion !</h3>
                <h4>Equipés de casque avec retour vidéo, les pilotes profitent d'une immersion aussi bluffante qu'impressionnante !</h4>
            </div>
        </div>
    </div>
</div>

<!-- map -->
<div class="section-container" id="carte">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 section-container-spacer">
                <h2 class="text-center">Carte</h2>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row map-container">
            <div id="maptt"></div>
        </div>
    </div>
</div>

<div class="section-container" id="contact-section-container">
    <div class="container contact-form-container">
        <div class="row">
            <div class="col-xs-12 col-md-offset-2 col-md-8">
                <div class="section-container-spacer">
                    <h2 class="text-center">Contactez-nous !</h2>
                </div>
                <div class="row">
                    <h3 class="text-center">Forum</h3>
                    <div class="col-md-12">
                        <p>Nous sommes habitués à échanger par le biais d'un forum, accessible ici :</p>
                        <p><a href="/forum/" target="_blank">https://www.rmcs.fr/forum/</a></p>
                    </div>
                </div>
                <div class="row">
                    <h3 class="text-center">Mail</h3>
                    <div class="col-md-12">
                        <p>Vous pouvez aussi nous joindre par mail : <a href="mailto:contact@rmcs.fr">contact@rmcs.fr</a></p>
                    </div>
                </div>
                <form action="" id="contactForm">
                    <div class="row">
                        <h3 class="text-center">Message</h3>
                        <div class="col-md-12">
                            <p>Il est possible également de nous envoyer un message grâce à ce formulaire :</p>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Nom">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="Email">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <textarea class="form-control" name="comment" rows="3" placeholder="Entrez votre message"></textarea>
                    </div>

                    <div class="form-group">
                        <img src='captcha.php?i=42' width=100px>
                        <label for="anti42">Anti-spam : De quel animal s'agit-il ? </label>
                        <select id="anti42" name="anti42">
                            <option value="" selected>Merci de choisir</option>
                            <option value="1">chien</option>
                            <option value="4">chat</option>
                            <option value="3">éléphant</option>
                            <option value="5">souris</option>
                            <option value="6">poule</option>
                            <option value="2">vache</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <img src='captcha.php?i=43' width=100px>
                        <label for="anti43">Anti-spam : De quel animal s'agit-il ? </label>
                        <select id="anti43" name="anti43">
                            <option value="" selected>Merci de choisir</option>
                            <option value="1">chien</option>
                            <option value="4">chat</option>
                            <option value="3">éléphant</option>
                            <option value="5">souris</option>
                            <option value="6">poule</option>
                            <option value="2">vache</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary">Envoyer</button>
                </form>
            </div>
        </div>
    </div>
</div>

<footer class="footer-container white-text-container">
    <div class="container">
        <div class="row">
                <div class="col-md-8">
                    <p>RMCS.fr | Radio Model Club Sénonais</p>
                    <p>Club RC / Drones / FPV / Voitures Tout Terrain à Sens de Bretagne</p>
                </div>

                <div class="col-md-4">
                    <div>
                        <p><small>© RMCS.fr</small></p>
                        <p><small>Site web créé avec <a href="http://www.mashup-template.com/">Mashup Template</a> / <a href="https://www.unsplash.com/">Unsplash</a></small></p>
                        <p><small>Licence Apache 2.0 / <a href='https://gitlab.com/arnaudmorin/rmcs.fr'>Code source sur Gitlab</a></small></p>
                    </div>
                </div>
        </div>
    </div>
</footer>

<script>
document.addEventListener("DOMContentLoaded", function (event) {
    
    scrollToAnchor();
    //scrollRevelation('reveal');

    // View/zoom differente si sur mobile
    var view = [48.33190, -1.5352];
    var zoom = 15;
    if (L.Browser.mobile) {
        zoom = 13;
    }
    var mymap = L.map('maptt', { dragging: !L.Browser.mobile, tap: !L.Browser.mobile, scrollWheelZoom: L.Browser.mobile }).setView(view, zoom);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYXJuYXVkbW9yaW4iLCJhIjoiY2tpaDhhZWpiMGY5YjMxbzN4ZG14OGp2NyJ9.QqV9qiYa49dDO7KbHbPPxg', {
        	maxZoom: 18,
        	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
        		'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        	id: 'mapbox/streets-v11',
        	tileSize: 512,
        	zoomOffset: -1,
                  scrollWheelZoom: false
        }).addTo(mymap);
  
    var markerTT = L.marker([48.33237, -1.55142]).addTo(mymap);
    markerTT.bindPopup("<b>Piste Privee TT</b><br>Ouvert les samedis apres-midi</br>a partir de 14h", {autoClose: false}).openPopup();

    var markerFPVa = L.marker([48.33263, -1.52485]).addTo(mymap);
    markerFPVa.bindPopup("<b>Drone / Helicopteres au Stade de la madeleine</b><br>Ouvert les samedis matin</br>a partir de 11h", {autoClose: false}).openPopup();

    navbarToggleSidebar();

    // Sendmail
    $("#contactForm").on('submit', function(event) {
        event.preventDefault(); 
        var formData = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: 'mail.php',
            dataType: "json",
            data: formData,
            success: function(response) { 
                alert(response.success); 
            },
            error: function(xhr, status, error){
                console.log(xhr); 
            }
        });
    });
});

</script>

<script type="text/javascript" src="./main.faaf51f9.js"></script>
</body>
</html>
