<?php
@session_start();

// Retrieve form data.
$name = $_POST['name'];
$email = $_POST['email'];
$comment = $_POST['comment'];
$anti42 = $_POST['anti42'];
$anti43 = $_POST['anti43'];
$errors = [];

// Simple server side validation for POST data, of course, you should validate the email
if (!$name) $errors[count($errors)] = "Merci d'indiquer votre nom !";
if (!$email) $errors[count($errors)] = "Merci d'indiquer votre  email!";
if (!$comment) $errors[count($errors)] = "Votre message est vide !";
if (!$anti42) $errors[count($errors)] = "Vous n'avez pas donne de reponse a l'anti-spam 1 !";
if (!$anti43) $errors[count($errors)] = "Vous n'avez pas donne de reponse a l'anti-spam 2 !";
if ($anti42 != $_SESSION['anti42']) $errors[count($errors)] = "Votre reponse anti-spam (1) est fausse !";
if ($anti43 != $_SESSION['anti43']) $errors[count($errors)] = "Votre reponse anti-spam (2) est fausse !";

// If the errors array is empty, send the mail
if (!$errors) {
    //recipient
    $to = 'Contact RMCS <contact@rmcs.fr>';
    //sender
    $from = $name . ' <' . $email . '>';

    //subject and the html message
    $subject = '[rmcs.fr] nouveau message de ' . $name;
    $message = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head></head>
    <body>
    <table>
            <tr><td>Nom</td><td>' . $name . '</td></tr>
            <tr><td>Email</td><td>' . $email . '</td></tr>
            <tr><td>Message</td><td>' . nl2br($comment) . '</td></tr>
    </table>
    </body>
    </html>';

    //send the mail
    $result = sendmail($to, $subject, $message, $from, $name, $email);

    // Unset code
    $_SESSION['anti42'] = '';
    $_SESSION['anti43'] = '';

    if ($result) {
        json_answer('Merci pour votre message !');
    }
    else {
        json_answer('Une erreur est survenu lors de l\'envoi du message, merci de reesayer plus tard.');
    }

//if the errors array has values
} else {
    //display the errors message
    $message = "";
    for ($i=0; $i<count($errors); $i++) $message.=$errors[$i] . "\n";
    json_answer($message);
}

function json_answer($message) {
    echo json_encode(array("success" => $message));
}

//Simple mail function with HTML header
function sendmail($to, $subject, $message, $from, $name, $email) {
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=utf-8" . "\r\n";
        //$headers .= 'From: ' . $from . "\r\n";
        $headers .= "From: rmcs.fr <brezhinnov@gmail.com>\r\n";
        $headers .= "Reply-To: $name <$email>\r\n";

        $result = mail($to,$subject,$message,$headers);

        if ($result) return 1;
        else return 0;
}
